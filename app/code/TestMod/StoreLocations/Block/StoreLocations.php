<?php
/**
 * TestMod_StoreLocations
 * 
 * @category TestMod
 * @package TestMod_StoreLocations
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */

    namespace TestMod\StoreLocations\Block;

    use Magento\Framework\View\Element\Template;


    /**
     * Class StoreLocations
     */
    class StoreLocations extends Template
    {
        /**
         * Constructor
         *
         * @param Magento\Framework\View\Element\Template\Context $context
         * @param TestMod\StoreLocations\Model\Data $model
         * @param Magento\Framework\Json\Helper\Data $jsonHelper
         */
        public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \TestMod\StoreLocations\Model\Data $model,
            \Magento\Framework\Json\Helper\Data $jsonHelper
        )
        {
            $this->model = $model;
            $this->jsonHelper = $jsonHelper;
            parent::__construct($context);
        }

        /**
         * Undocumented function
         *
         * @return string
         */
        public function getCreateMassege()
        {
            return 'Module Created Successfully';
        }

        /**
         * Undocumented function
         *
         * @return collection
         */
        public function getCollection()
        {
            $storeCollection = $this->model->getCollection();
            return $storeCollection;
        }

        public function getApiKey()
        {
            return 'AIzaSyDUciZSJ4Iw7cm1zFbBPvjL0vWZGcDPoEM';
            //return $this->_scopeConfig->getValue('path/to/config');
        }

        public function getCollectionJson()
        {
            $storeCollection = $this->model->getCollection();
            $result = [];
            foreach ($storeCollection as $item) {
                $result[] = 
                [
                    'id' => $item->getId(), 
                    'name' => $item->getName(), 
                    'address' => $item->getAddress(),
                    'lat' => $item->getLat(), 
                    'lng' => $item->getLng(), 
                    'tel' => $item->getTel(),
                    'm_tel' => $item->getM_tel(), 
                    'mon' => $item->getMon(), 
                    'tue' => $item->getTue(),
                    'wed' => $item->getWed(), 
                    'thu' => $item->getThu(), 
                    'fri' => $item->getFri(),
                    'sat' => $item->getSat(), 
                    'sun' => $item->getSun()
                ];
            }
            $this->jsonHelper->jsonEncode($result);
            return $result;
        }
    }
?>