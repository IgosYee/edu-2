<?php
/**
 * TestMod_StoreLocations
 * 
 * @category TestMod
 * @package TestMod_StoreLocations
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */

namespace TestMod\StoreLocations\Block\Adminhtml\Stores\Edit;

/**
 * Class AddStore
 */
class AddStore extends Generic
{
    /**
     * {@inheritdoc}
     */
    public function getButtonData()
    {
        return [
            'label' => __('Add Store'),
            'class' => 'action',
            'data_attribute' => [
                'mage-init' => [
                    'Magento_Ui/js/form/button-adapter' => [
                        'actions' => [
                            [
                                'targetName' => 'product_form.product_form.add_attribute_modal',
                                'actionName' => 'toggleModal'
                            ]
                        ]
                    ]
                ]
            ],
            'on_click' => '',
            'sort_order' => 20
        ];
    }
}