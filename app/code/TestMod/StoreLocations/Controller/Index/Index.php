<?php
/**
 * TestMod_StoreLocations
 * 
 * @category TestMod
 * @package TestMod_StoreLocations
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */

    namespace TestMod\StoreLocations\Controller\Index;

    /**
     * Class Index
     */
    class Index extends \Magento\Framework\App\Action\Action
    {
        /**
        * @var \Magento\Framework\View\Result\PageFactory
        */
        protected $resultPageFactory;


        /**
        * @param \Magento\Framework\App\Action\Context $context
        * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
        */
        public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory
        )
        {
        $this->resultPageFactory = $resultPageFactory;
            parent::__construct($context);
        }

        
        /**
        * Default customer account page
        *
        * @return void
        */
        public function execute()
        {
            return $this->resultPageFactory->create();
        }
    }
?>