<?php
/**
 * TestMod_StoreLocations
 * 
 * @category TestMod
 * @package TestMod_StoreLocations
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */

namespace TestMod\StoreLocations\Controller\Adminhtml\Stores;


use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use TestMod\StoreLocations\Model\ResourceModel\Data\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;

class Save extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            try{
                $id = $data['id'];
                $customModel = $this->customModel->load($id);
                $data = array_filter($data, function($value) { return $value !== ''; });
                $customModel->setData($data);
                $customModel->save();

                $this->messageManager->addSuccess(__('Successfully saved the item.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);                
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {               
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $customModel->getId()]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}