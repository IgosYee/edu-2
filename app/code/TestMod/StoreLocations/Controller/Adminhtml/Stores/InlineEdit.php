<?php
/**
 * TestMod_StoreLocations
 * 
 * @category TestMod
 * @package TestMod_StoreLocations
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */

namespace TestMod\StoreLocations\Controller\Adminhtml\Stores;


use Magento\Framework\Controller\Result\JsonFactory;


class InlineEdit extends \TestMod\StoreLocations\Controller\Adminhtml\Stores
{
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * InlineEdit constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Eadesigndev\Warehouses\Model\StockItemsRepository $itemModel
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context, $resultPageFactory);
    }
    /**
     * @return $this
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);

            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } 
            else {
                foreach (array_keys($postItems) as $itemsId) {
                    $items = $this->itemModel->getById($itemsId);

                    $items->setData(array_merge($items->getData(), $postItems[$itemsId]));
                    $this->itemModel->save($items);
                }
            }
        }
        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}