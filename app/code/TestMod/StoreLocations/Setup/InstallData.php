<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace TestMod\StoreLocations\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
          /**
           * Install messages
           * 
           */
          $data = [
            ['name' => 'Jysk',
            'address' => 'Rīga, Brīvības gatve 372',
            'lat' => 56.948581,
            'lng' => 24.108154,
            'tel' => '12345678',
            'm_tel' => '987654321',
            'mon' => '10:00 - 22:00',
            'tue' => '10:00 - 22:00',
            'wed' => '10:00 - 22:00',
            'thu' => '10:00 - 22:00',
            'fri' => '12:00 - 24:00',
            'sat' => '11:00 - 20:00',
            'sun' => '11:00 - 20:00']
          ];
          foreach ($data as $bind) {
              $setup->getConnection()
                ->insertForce($setup->getTable('testmod_storeinfo'), $bind);
          }
    }
}