<?php
/**
 * TestMod_StoreLocations
 * 
 * @category TestMod
 * @package TestMod_StoreLocations
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */

namespace TestMod\StoreLocations\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'testmod_storeinfo'
         */
        $table = $installer->getConnection()
            // Get storelocations_data table
            ->newTable($installer->getTable('testmod_storeinfo'))
            // store_id column
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Stock Name'
            )
            ->addColumn(
                'address',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Shop address'
            )
            ->addColumn(
                'lat',
                \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
                10.6,
                ['nullable' => false],
                'Stock Latitude'
            )
            ->addColumn(
                'lng',
                \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
                10.6,
                ['nullable' => false],
                'Stock Longitude'
            )
            ->addColumn(
                'tel',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Shop Telephone Number'
            )
            ->addColumn(
                'm_tel',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Shop Mobile Telephone Number'
            )
            ->addColumn(
                'mon',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Working Hours At Monday'
            )
            ->addColumn(
                'tue',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Working Hours At Tuesday'
            )
            ->addColumn(
                'wed',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Working Hours At Wednesday'
            )
            ->addColumn(
                'thu',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Working Hours At Thursday'
            )
            ->addColumn(
                'fri',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Working Hours At Friday'
            )
            ->addColumn(
                'sat',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Working Hours At Saturday'
            )
            ->addColumn(
                'sun',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Working Hours At Sunday'
            )
            ->setComment('Cataloginventory Stock');
        $installer->getConnection()
            ->createTable($table);

        $installer->endSetup();
    }
}
