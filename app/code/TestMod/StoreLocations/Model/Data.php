<?php
/**
 * TestMod_StoreLocations
 * 
 * @category TestMod
 * @package TestMod_StoreLocations
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */

namespace TestMod\StoreLocations\Model;
	 
use Magento\Framework\Model\AbstractModel;
	 
class Data extends AbstractModel
{	
    protected function _construct()
    {
        $this->_init('TestMod\StoreLocations\Model\ResourceModel\Data');
    }
}