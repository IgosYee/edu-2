/**
 * TestMod_StoreLocations
 * 
 * @category TestMod
 * @package TestMod_StoreLocations
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */

/**
 * Initialize the map
 * 
 * @param json storesInfoJson
 */
function initMap(storesInfoJson) {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: new google.maps.LatLng(56.944205, 24.131200),
    zoom: 12
  });
  console.log(storesInfoJson);
  downloadMarkers(map, storesInfoJson);
};


/**
 * @returns document.element
 * 
 * @param string name 
 * @param string address 
 */
function createMarkerInfo(name, address){
  var strong = document.createElement('strong');
  strong.className += ' list__location-adress list__location-addres_active ';
  strong.textContent = name;

  var text = document.createElement('text');
  text.textContent = address;
  
  var content = document.createElement('div');
  content.appendChild(strong);
  content.appendChild(document.createElement('br'));
  content.appendChild(text);

  return content;
}

/**
 * @returns document.element
 * 
 * @param int id
 * @param string name 
 * @param string address 
 */
function createListInfo(id, name, address){
  var div = document.createElement('div');
  div.className += ' list__location-adress ';
  div.textContent = name;

  var text = document.createElement('text');
  text.textContent = address;
  
  var content = document.createElement('div');
  content.setAttribute('class', ' list__item ');
  content.setAttribute('data-shop_id', id);
  content.setAttribute('id', 'listItem');
  content.appendChild(div);
  content.appendChild(document.createElement('br'));
  content.appendChild(text);

  return content;
}

/**
 * @param google.maps.Map map
 * @param json storesInfoJson
 */
function downloadMarkers(map, storesInfoJson){
  require([ "jquery" ], function($){
    var infoWindow = new google.maps.InfoWindow;
    var list = document.getElementById('shopsList');

    for (var i = 0; i < storesInfoJson.length; i++){
      let store = storesInfoJson[i];

      let id = store['id'];
      let name = store['name'];
      let address = store['address'];
      let type = store['type'];
      let point = new google.maps.LatLng
      (
          parseFloat(store['lat']),
          parseFloat(store['lng'])
      );

      let marker = new google.maps.Marker({
        id: id,
        map: map,
        position: point,
        label: id
      });

      let infomapcontent = createMarkerInfo(name, address);
      let infolistcontent = createListInfo(id, name, address);
      list.appendChild(infolistcontent);

      marker.addListener('click', function() {
        //select shop on the map
        infoWindow.setContent(infomapcontent);
        infoWindow.open(map, marker);
        //select shop in the list
        $('#listItem.list__location-addres_active').removeClass('list__location-addres_active');
        infolistcontent.className += ' list__location-addres_active ';
        //infolistcontent.scrollIntoView();
      });

      infolistcontent.addEventListener('click', function() {
        //select shop on the map
        infoWindow.setContent(infomapcontent);
        infoWindow.open(map, marker);
        //select shop in the list
        $('#listItem.list__location-addres_active').removeClass('list__location-addres_active');
        infolistcontent.className += ' list__location-addres_active ';
      });

    };
  });
};