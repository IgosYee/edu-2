/**
 * TestVen_TestTheme
 * 
 * @category TestVen
 * @package TestVen_TestTheme
 * @author Igors Kolcins <igorskolchins@gmail.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license http://opensource.org/licenses/OSL-3.0 The Ope Software License 3.0 (OSL-3.0)
 */


require([ "jquery" ], function($){
    
    /**
     * @var element
     */
    var $header = $('.page-header');

    /**
     * @var element
     */
    var $clone = $header.before($header.clone().addClass("clone"));

    /**
     * @var int
     */
    var headerContentHeight = $clone.children('.header.content').outerHeight(true);

    /**
     * On scroll add transform style
     */
    $(window).scroll(function () {
        let fromTop = $(window).scrollTop();
        if (fromTop > 400){
            $('.clone').css({'transform': 'translateY('+ (-headerContentHeight) +'px)'});
        }
        else
        {
            $('.clone').css({transform: 'translateY(0)'});
        }
    });

});