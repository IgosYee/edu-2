# EDU-137
The educational tasks.  
- [2. Content FE Styling (CMS content)](#markdown-header-edu137-2)  
- [5. Custom UI JavaScript (Sticky Header)](#markdown-header-edu137-5)  
- [8. Custom module functionality with new DB table (Store locator)](#markdown-header-edu137-8)
---
## EDU137-2
## 2. Content FE Styling (CMS content)
This was a little bit hard, because until this moment about the magento I just heard. 
First of all I made a new theme, then I looked for how to properly connect my styles to parent styles. 
Then in the admin panel I added a new page and wrote the HTML code in the same place.
### How i tested:
1. I opened page www.newmagento.com/first-task
2. All working OK
3. F12 -> Toogle device toolbar -> Galaxy S5
4. Refresh the page
5. All working OK
6. Try same thing in IE and FF
### Mistakes
I did it with .less, not a .sass... Аfter other tasks I will return and fix it.
### Files
```
app/design/frontend/TestVen/TestTheme // Theme folder
├─ 	theme.xml
├─ 	registration.php
├─ 	./Magento_Theme/layout/default.xml
├─ 	./web/css/source/
│   ├─ _extend.less
│   └─ _first_task_theme.less
├─ 	./web/fonts/OpenSans-Regular.ttf
└─ 	./web/images/
    ├─ waterlemon-logo.svg
	└─ icons.png
```
---
## EDU137-5
## 5. Custom UI JavaScript (Sticky Header)
Not a very difficult task. For this task I try to change *default.xml* and it was very unclear how to do it. 
After I just create css file and js file where I realized the functional, and add it to *default_head_blocks.xml*. In my opinion, the realization is not very good, 
because I hardcoded the header classes in jquery selectors.
### How i tested:
1. I opened page www.newmagento.com/first-task
2. Scroll up and down with mouse, scrolline and arrows.
3. All working OK
4. F12 -> Toogle device toolbar -> Galaxy S5
5. Refresh the page
6. Scrolling -> OK
7. Try same thing in IE and FF
### Files
```
app/design/frontend/TestVen/TestTheme // Theme folder
├─ 	./Magento_Theme/layout/default_head_blocks.xml
├─ 	./Magento_Theme/layout/default.xml
├─ 	./web/css/source/
│   └─ sticky-header.css
└─ 	./web/js/
    └─ sticky-header.js
```
---
## EDU137-8
## 8. Custom module functionality with new DB table (Store locator)
It was very difficult. I divided the assignment into two parts: FE and BE. First I created a new module from the database, and then FE (styles and js).
This part was simple, but the second part ... 
Spent a lot of time on Ui Grid and Ui Forms, and I think I could do better. The most difficult thing is to undestand how data is loaded.
### How i tested:
1. I opened page www.newmagento.com/storelocations
2. Scroll up and down with mouse, scrolline and arrows.
3. Click on the shop in list section, the clik on the marker in the map section.
4. F12 -> Toogle device toolbar -> Galaxy S5
5. Refresh the page
6. Repeat 2 - 3 steps
7. Try same thing in IE and FF (1 - 6 steps)
8. www.newmagento.com/admin/storelocations/stores
9. Inser new row x3
10. Delete one row, and delete 2 others at once
### Need to fix
InlineEdit doesn't work and searching too.
### Files
```
app/code/TestMod/StoreLocations // Module folder
```